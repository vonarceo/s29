/*
	VON ARCEO
	B210FT
	S29-ACTIVITY
*/

// NO.2)


db.users.find({
	$or: [
	{
		firstName: {
			$regex: "s",
			$options: "$i"
		}
	},
	{
		lastName: {
			$regex: "d",
			$options: "$i"
		}
	}
	]
},
{
		_id: 0,
		age: 0,
		contact: 0,
		courses: 0,
		department: 0
}
)



// NO.3

db.users.find({
	$and: [
	{
		department: "HR"
	},
	{
		age: {
			$gte: 70
		}
	}
	]
})

// NO.4

db.users.find({
	$and: [
		{
		firstName: {
			$regex: "e",
			$options: "$i"
		}
		},
		{
		age: {
			$lte: 30
		}	
		}
	]
})


